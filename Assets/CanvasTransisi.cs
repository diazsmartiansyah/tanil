﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CanvasTransisi : MonoBehaviour
{
    public static string NameScene;
    public static string sceneBefore;


    public void btn_pindah(string nama)
    {

        this.gameObject.SetActive(true);
        NameScene = nama;
        SceneManager.LoadScene(NameScene);
        GetComponent<Animator>().Play("end");
    }


    public void Object_InActive()
    {
        this.gameObject.SetActive(false);
    }

    public void Pindah_Scene()
    {
        SceneManager.LoadScene(NameScene);
    }


    public void btn_keluargame()
    {
        Application.Quit();
    }

    // Load the previous scene
    public void LoadPreviousScene()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int previousSceneIndex = currentSceneIndex - 1;

        if (previousSceneIndex >= 0)
        {
            SceneManager.LoadScene(previousSceneIndex);
        }
        else
        {
            // Optionally, handle the case where there's no previous scene to go back to
            Debug.LogWarning("There is no previous scene.");
        }
    }

}
